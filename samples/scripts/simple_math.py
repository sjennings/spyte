import time
import json
import spyte.logger as logger
from spyte.executor import TestExecutor

def get_errors(result):
  errors = []

  for error in result["errors"]:
    errors.append("[%s] %s" %(error["type"], error["msg"]))

  return errors

def prestep_cb(stepdef, paths, params):
  msg = "%s: %s" %(stepdef["desc"], str(params))
  if len(paths) < 3:
    logger.bold("\n" + msg)
  else:
    logger.info(msg) 

def poststep_cb(stepdef, paths, result):
  if result["success"]:
    if len(paths) == 2:
      logger.success("PASS")
  else:
    for error in get_errors(result):
      logger.error(error)

def prefunc_cb(paths, args, kwargs):
  msg = "%s()" %(paths[-1])
  if args:
    msg = msg + " args = %s" %(str(args))
  if kwargs:
    msg = msg + " kwargs = %s" %(str(kwargs))
  logger.info(msg) 

def postfunc_cb(paths, result):
  if result["success"]:
    logger.success("OK")
  else:
    for error in get_errors(result):
      logger.error(error)

class SimpleMath:
  def _check_type_(self, val1, val2):
    if not isinstance(val1, int):
      raise Exception("%s is not an integer" %(str(val1)))
    elif not isinstance(val2, int):
      raise Exception("%s is not an integer" %(str(val2)))

  def add(self, val1, val2):
    self._check_type_(val1, val2)
    return val1 + val2

  def mult(self, val1, val2):
    self._check_type_(val1, val2)
    return val1 * val2

e = TestExecutor("/tmp/testdefs",
                 prestep_cb,
                 poststep_cb,
                 prefunc_cb,
                 postfunc_cb,
                 simple_math = SimpleMath())

e.run("defs.simple_math.test_all", a=8, b=2)

logger.stop(timeout = 5)
