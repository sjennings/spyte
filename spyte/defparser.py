import os
import json

from spyte.exceptions import DefParserError

class DefParser:
    '''
    This class is used to parse definition files in the given directory.
    '''
    def __init__(self, defpath):
        self.defpath = defpath
        if not os.path.exists(self.defpath):
            msg = "Def directory does not exist"
            try:
                msg = msg + ": %r" %(self.defpath)
            except Exception:
                pass
            raise DefParserError(msg)

    def _defclean_(self, attr):
        '''
        This function recursinely crawls through a test definition, stripping 
        comments from it (dictionary elements with an empty string as a key),
        and searching for dictionary keys that end with `%`, expecting the
        value to be an array of strings which are then concatenated together to
        form a single  string. If the value is not a list or the list contains
        elements that are not strings the key-value pair is left unaltered.
        Otherwise, the `%` is removed from the key and the value is the
        concatenated string. For example:

          `{"test%": ["This ", "is a", " test."]}`

        Becomes:

          `{"test": "This is a test."}`.
        
        @param attr - the attribute containing the test definition
        '''
        newattr = attr

        if isinstance(attr, dict):
            newattr = {}
            for key, val in attr.iteritems():
                if key == "":
                    continue

                try:
                    assert key[-1] == "%"
                    assert key[-2] != "%"
                    assert isinstance(val, list)

                    newval = ""
                    for string in val:
                        newval = newval + string
                    
                    newattr[key[:-1]] = newval

                except Exception:
                    newattr[key] = self._defclean_(val)
                    
        elif isinstance(attr, list):
            newattr = []
            for val in attr:
                newattr.append(self._defclean_(val))
                    
        return newattr

    def parse_file(self, filename = None, filepath = None):
        f = None
        error = None
        try:
            if filename:
                filepath = "%s/%s" %(self.defpath, filename)

            f = open(filepath, "r")
            def_ = json.loads(f.read())

        except IOError as e:
            msg = "Unable to open def file"
            try:
                msg = msg + ", " + str(filepath)
                msg = msg + ": " + str(e)
            except Exception:
                pass
            error = DefParserError(msg)

        except Exception as e:
            msg = "JSON decoding failed"
            try:
                msg = msg + " in " + str(filepath)
                msg = msg + ": " + str(e)
            except Exception:
                pass
            error = DefParserError(msg)

        if f:
            f.close()

        if error:
            raise error

        if not isinstance(def_, dict):
            msg = "File does not contain a JSON object"
            try:
                msg = msg + ": " + str(filepath)
            except Exception:
                pass
            raise DefParserError(msg)

        return self._defclean_(def_)

    def parse_all(self, extensions = [".json"]):
        defs = {}
        for filename in os.listdir(self.defpath):
            name, extension = os.path.splitext(os.path.basename(filename))
            if extension in extensions:
                defs[name] = self.parse_file(filename)
        return defs
