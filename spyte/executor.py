from exceptions import *

import os
import sys
import json
import time
import traceback
import copy

import spyte.logger as logger

from spyte.defparser import DefParser

DEBUG = True

class _Attr_:
    '''
    This is an empty class used to hold functions and attributes passed to the
    TestExecutor.
    '''
    pass

class _TestDefParser_(DefParser):

    def _esc_find_(self, string, char, start = 0, esc = "\\"):
        string_ = self._include_split_(string[start:], esc + char)
        index = start
        
        for chunk in string_:
            if chunk != (esc + char):
                i = chunk.find(char)
                if i >= 0:
                    return index + i
            index = index + len(chunk)

        return -1

    def _resolve_string_(self, string, refs):
        _valid_list_ = [
            "defs",
            "attr",
            "params",
            "vars",
            "retvals"]
        chunks = []
        start = 0
        end = -1
        while True:
            # Find refs enclosed in angle brackets
            start = self._esc_find_(string, "<", end+1)

            if start < 0:
                chunks.append(string[end+1:])
                break
            else:
                chunks.append(string[end+1:start])

            end = self._esc_find_(string, ">", start)
            
            if end < 0:
                chunks.append(string[start:])
                break

            # Parse format and reference path
            fmt, ref = self.parse_ref(string[start:end+1])
            ref_ = self.esc_split(ref, ".")
            reftype = ref_[0]

            # Make sure reference type is valid
            if not reftype in _valid_list_:
                msg = "Invalid reference"
                try:
                    msg = msg + ", must start with one of the following: "
                    msg = msg + str(", ".join(_valid_list_))
                    msg = msg + "-- not `%s`" %(reftype)
                    msg = msg + " in `%s`" %(string)
                except Exception:
                    pass
                raise SpyteError(msg)

            # Resolve reference
            try:
                val = refs[reftype]
                del ref_[0]
                while len(ref_) > 0:
                    if reftype == "attr":
                        val = getattr(val, ref_[0])
                    elif isinstance(val, list):
                        val = val[int(ref_[0])]
                    else:
                        val = val[ref_[0]]
                    del ref_[0]

            except Exception as e:
                msg = "Failed to resolve reference"
                try:
                    msg = msg + ": " + str(ref)
                    msg = msg + ": " + str(e)
                except Exception:
                    pass
                raise SpyteError(msg)

            # Apply reference format
            try:
                if len(fmt) > 0:
                    val = fmt %(val)

            except Exception:
                msg = "Invalid reference format"
                try:
                    msg = msg + ", `%s`" %(fmt)
                    msg = msg + ", for `%s`" %(ref)
                    msg = msg + ", which resolved to %r" %(val)
                except Exception:
                    pass
                raise SpyteError(msg)

            # Check dereferenced strings for additional references
            if isinstance(val, unicode) or isinstance(val, str):
                chunks.append(self._resolve_string_(val, refs))

            # Make sure non-string references don't appear in the middle of
            # other strings
            elif start == 0 and end == (len(string) - 1):
                return val
            else:
                msg = "Mid-string reference missing format"
                try:
                    msg = msg + ": `%s`" %(ref)
                except Exception:
                    pass
                raise SpyteError(msg)

        return "".join(chunks)

    def _check_def_(self,
                    attr,
                    valid_keys = [],
                    required_keys = [],
                    path = ""):

        # Make sure attr is a dict
        if not isinstance(attr, dict):
            msg = "Definition is not a JSON object"
            try:
                msg = msg + path
            except Exception:
                pass
            raise SpyteError(msg)

        # Check for invalid keys
        for key in attr:
            if not key in valid_keys:
                msg = "Invalid key"
                try:
                    msg = msg + ": `%s`" %(key)
                    msg = msg + path
                except Exception:
                    pass
                raise SpyteError(msg)

        # Check for missing keys
        for key in required_keys:
            if not key in attr:
                msg = "Missing key"
                try:
                    msg = msg + ": `%s`" %(key)
                    msg = msg + path
                except Exception:
                    pass
                raise SpyteError(msg)

    def _check_substepdef_(self, attr, path = ""):
        # Check general keys of substep def
        self._check_def_(attr,
                         valid_keys = [
                             "type",
                             "set",
                             "path",
                             "params",
                             "args",
                             "kwargs",
                             "expected_result",
                             "retpath",
                             "cond",
                             "timeout",
                             "substep",
                             "script"
                         ],
                         required_keys = ["type"],
                         path = path)

        # Check type-specific keys in substep def 
        if (attr["type"] == "and" or
            attr["type"] == "or" or
            attr["type"] == "all"):
            keys = ["type", "set"]

        elif attr["type"] == "func":
            keys = [
                "type", "path", "args", "kwargs", "expected_result", "retpath"]

        elif attr["type"] == "step":
            keys = ["type", "path", "params"]

        elif attr["type"] == "if":
            keys = ["type", "cond", "substep"]

        elif attr["type"] == "loop":
            keys = ["type", "timeout", "substep"]

        elif attr["type"] == "eval":
            keys = ["type", "script", "expected_result", "retpath"]

        else:
            msg = "Unknown substep type"
            try:
                msg = msg + ": " + repr(attr["type"])
                msg = msg + path
            except Exception:
                pass
            raise SpyteError(msg)

        self._check_def_(attr,
                         valid_keys = keys,
                         required_keys = keys,
                         path = path)

        # Recursively check steps that contain multiple substeps
        if (attr["type"] == "and" or
            attr["type"] == "or" or
            attr["type"] == "all"):
            for substepdef in attr["set"]:
                self._check_substepdef_(substepdef,
                                        " in `%s`%s" %(attr["type"], path))

    def _include_split_(self, string, delim):
        string_ = string.split(delim)
        string_delim = [string_[0]]
        for i in range(1,len(string_)):
            string_delim.append(delim)
            string_delim.append(string_[i])
        return string_delim

    def _recursive_include_split_(self, list_, delim, ignore = []):
        list_delim = []
        for element in list_:
            if element in ignore:
                list_delim.append(element)
            else:
                list_delim = list_delim + self._include_split_(element, delim)
        return list_delim

    def esc_split(self, string, delim, esc = "\\"):
        string_delim = self._recursive_include_split_(
            self._recursive_include_split_(
                self._include_split_(
                    string, esc + esc),
                esc + delim, ignore = [esc + esc]),
            delim, ignore = [esc + esc, esc + delim])

        i = 0
        while i < len(string_delim):
            if string_delim[i] == delim:
                del string_delim[i]
            else:
                i = i + 1

        return string_delim

    def parse_ref(self, ref):
        try:
            ref_ = ref

            if ref_[0] == "<":
                ref_ = ref_[1:]

            if self.esc_split(ref_, ">")[-1] == "":
                ref_ = ref_[:-1]

            ref_ = self.esc_split(ref_, ":")

            return (ref_[0], ":".join(ref_[1:]))

        except Exception as e:
            msg = "Failed to parse reference"
            try:
                msg = msg + ": " + str(ref)
            except Exception:
                pass
            raise SpyteError(msg)

    def parse_file(self, filename = None, filepath = None):
        testdef = DefParser.parse_file(self, filename, filepath)

        for stepname, stepdef in testdef.iteritems():
            path = ""
            try:
                path = path + " in " + str(stepname)
                path = path + " in " + str(filepath)
            except Exception:
                pass

            keys = ["desc", "params", "vars", "substep"]
            self._check_def_(stepdef,
                             valid_keys = keys,
                             required_keys = keys,
                             path = path)
            self._check_substepdef_(stepdef["substep"], path)
        return testdef

    def resolve(self, attr, refs):
        if isinstance(attr, str) or isinstance(attr, unicode):
            newattr = self._resolve_string_(attr, refs)

        elif isinstance(attr, list):
            newattr = []
            for val in attr:
                newattr.append(self.resolve(val, refs))

        elif isinstance(attr, dict):
            newattr = {}
            for key, val in attr.iteritems():
                newattr[self.resolve(key, refs)] = self.resolve(val, refs)

        else:
            newattr = attr

        return newattr

class TestExecutor:
    def __init__(self,
                 _defpath_,
                 _preexec_cb_ = None,
                 _postexec_cb_ = None,
                 **kwargs):
        '''
        Initializes test executor.
        
        @param _defpath_ -       Path to directory containing *.json test step
                                 definition files.
        @param _preexec_cb_ -    (Optional) Callback that is called immediately
                                 prior to running a test step of type `func`,
                                 `eval` or `step`. See prototype 
                                 self.preexec_cb().
        @param _postexec_cb_ -   (Optional) Callback that is called immediately
                                 after a test step of type `func`, `eval`, or
                                 `step` has finished running. See prototype
                                 self.postexec_cb().
        @param **kwargs -        Pass any number of key value pairs after
                                 required and optional parameters to initialize
                                 the test executor. Each item then becomes
                                 self.<key> = <value>.
        '''
        self._defpath_ = _defpath_
 
        if _preexec_cb_:
            self.preexec_cb = _preexec_cb_

        if _postexec_cb_:
            self.postexec_cb = _postexec_cb_

        self._parser_ = _TestDefParser_(self._defpath_)

        self.steps = []
        self.defs = self._parser_.parse_all()
        self.attr = _Attr_()
        self.retvals = {}

        for key, attr in kwargs.iteritems():
            setattr(self.attr, key, attr)

    def _is_subset_(self, set1, set2):        
        if set1 == None:
            pass

        elif isinstance(set1, dict) and isinstance(set2, dict):
            for key in set1:
                if not key in set2:
                    return False
                if not self._is_subset_(set1[key], set2[key]):
                    return False

        elif isinstance(set1, list) and isinstance(set2, list):
            offset = 0
            i = 0
            while True:
                if i >= len(set1):
                    break
                elif i + offset >= len(set2):
                    return False
                elif self._is_subset_(set1[i], set2[i+offset]):
                    i = i + 1
                else:
                    offset = offset + 1

        elif ((isinstance(set1, unicode) or isinstance(set1, str)) and 
              (isinstance(set2, unicode) or isinstance(set2, str))):
            if not unicode(set1) in unicode(set2):
                return False
 
        elif set1 != set2:
            return False

        return True

    def _get_filename_(self, filepath):
        return os.path.splitext(os.path.basename(filepath))[0]

    def _add_testdef_(self, filename = None, filepath = None):
        testdef = self._parser_.parse_file(filename, filepath)
        if filepath:
            self.defs[self._get_filename_(filepath)] = testdef
        else:
            self.defs[self._get_filename_(filename)] = testdef
        return testdef

    def _check_result_(self, substepdef):
        if (substepdef["expected_result"] != None and
            not self._is_subset_(
                substepdef["expected_result"], substepdef["result"])):

            msg = "Unexpected result"
            try:
                msg = msg + " for %s" %(str(substepdef["paths"][-1]))
                msg = msg + (", expected %s `%r`"
                             %(str(type(substepdef["expected_result"])),
                               substepdef["expected_result"]))
                msg = msg + (", got %s `%r`"
                             %(str(type(substepdef["result"])),
                               substepdef["result"]))
            except Exception:
                pass
            raise SpyteError(msg)

    def _resolve_error_(self, error):
        trace = []
        try:
            trace = traceback.format_exception(*sys.exc_info())
        except Exception:
            pass

        return {
            "type": type(error).__name__,
            "msg": str(error),
            "trace": trace,
        }

    def _save_retval_(self, substepdef):
        if substepdef["retpath"] == None:
            return

        if not (isinstance(substepdef["retpath"], unicode) or
                isinstance(substepdef["retpath"], str)):
            msg = "Invalid retpath"
            try:
                msg = msg + ": " + repr(retpath)
            except Exception:
                pass
            raise SpyteError(msg)

        retpath = self._parser_.esc_split(substepdef["retpath"], ".")
            
        # Build the path in the dict
        attr = self.retvals
        for i in range(0,len(retpath)):
            create_node = False
            if isinstance(attr, dict):
                create_node = (not retpath[i] in attr)
            elif isinstance(attr, list):
                try:
                    create_node = (not int(retpath[i]) in attr)
                except Exception:
                    msg = "Invalid reference key"
                    try:
                        msg = msg + ", `%s`" %(str(retpath[i]))
                        msg = msg + ", in `%s`" %(str(substepdef["retpath"]))
                    except Exception:
                        pass
                    raise SpyteError(msg)
            else:
                msg = "Cannot resolve ref in non-dict/list object"
                try:
                    msg = msg + ", `%r`" %(attr)
                    msg = msg + ", at `%s`" %(str(retpath[i]))
                    msg = msg + " in " + str(substepdef["retpath"])
                except Exception:
                    pass
                raise SpyteError(msg)

            if i == (len(retpath) - 1):
                attr[retpath[i]] = substepdef["result"]

            elif create_node:
                attr[retpath[i]] = {}

            attr = attr[retpath[i]]

        return attr

    def _get_deps_(self, path = None, filepath = None, substep = None):
        deps = []
        deps_ = []

        if path:
            deps_ = deps_ + [path]
            stepdef = self.resolve(path)
            if isinstance(stepdef, dict) and "substep" in stepdef:
                deps_ = deps_ + self._get_deps_(substep = stepdef["substep"])

        if filepath:
            testdef = self._parser_.parse_file(filepath = filepath)
            for _, stepdef in testdef.iteritems():
                deps_ = deps_ + self._get_deps_(substep = stepdef["substep"])

        if substep:
            if "path" in substep:
                deps_ = deps_ + self._get_deps_(path = substep["path"])
            elif "set" in substep:
                for substep_ in substep["set"]:
                    deps_ = deps_ + self._get_deps_(substep = substep_)
            elif "substep" in substep:
                deps_ = deps_ + self._get_deps_(substep = substep["substep"])

        for dep in deps_:
            if not dep in deps:
                deps.append(dep)

        return deps

    def _resolve_path_(self, path):
        try:
            stepdef = copy.copy(self.resolve(path))
            assert(isinstance(stepdef, dict))
            stepdef["type"] = "step"
            stepdef["path"] = path
            return stepdef
        except Exception as error:
            msg = "Failed to resolve path to a valid test step"
            try:
                msg = msg + ": %r" %(path)
            except Exception:
                pass

            try:
                msg = msg + ": %r" %(error)
            except Exception:
                pass

            raise SpyteError(msg)

    def _override_attr_(self, attr, override):
        if isinstance(attr, dict) and isinstance(override, dict):
            new_attr = copy.copy(attr)
            for key, val in attr.iteritems():
                if key in override:
                    new_attr[key] = self._override_attr_(val, override[key])
        else:
            new_attr = copy.copy(override)

        return new_attr

    def _merge_attr_(self, attr, secondary):
        if isinstance(attr, dict) and isinstance(secondary, dict):
            new_attr = copy.copy(secondary)
            for key, val in attr.iteritems():
                if key in secondary:
                    new_attr[key] = self._merge_attr_(val, secondary[key])
                else:
                    new_attr[key] = val
        else:
            new_attr = copy.copy(attr)

        return new_attr

    def _resolve_step_(self,
                       path = None,
                       stepdef = None,
                       parent = None,
                       params = {},
                       vars_ = {}):

        substep_params = {}
 
        # If this is a substep of type 'step', then we want to save the params,
        # vars, and path for step definition lookup
        if (isinstance(stepdef, dict) and
            "type" in stepdef and
            "path" in stepdef and 
            stepdef["type"] == "step"):

            path = stepdef["path"]
            substep_params = self.resolve(stepdef["params"], params, vars_)

        # If a path is provided, we assume that stepdef is not and must be
        # looked up in self.defs
        if path:
            stepdef = self._resolve_path_(path)

        # Prepare return structure
        step = copy.copy(stepdef)
        step["parent"] = None
        step["paths"] = []
        step["result"] = None
        step["errors"] = []
        step["success"] = None
        step["index"] = len(self.steps)
        self.steps.append(step)

        try:
            step["parent"] = parent["index"]
            step["paths"] = copy.copy(parent["paths"])
        except Exception:
            pass

        if "path" in step:
            step["paths"].append(step["path"])
            del step["path"]

        # Pass applicable params and vars from caller to this step
        if "params" in step:
            if step["type"] == "step":
                step["params"] = self._override_attr_(
                    step["params"], substep_params)

            if parent == None:
                step["params"] = self._override_attr_(
                    step["params"], params)

        else:
            step["params"] = params
        
        if not "vars" in step:
            step["vars"] = vars_

        # Resolve refs that depend on caller params
        for key in [
                "params",
                "vars",
                "args",
                "cond",
                "expected_result",
                "kwargs",
                "retpath",
                "script",
                "timeout"]:

            if key in step:
                try:
                    step[key] = self.resolve(
                        step[key], params, vars_)
                except Exception as error:
                    step["errors"].append(self._resolve_error_(error))
                    step["success"] = False

        # Resolve refs that depend on step params
        for key in ["desc"]:
            if key in step:
                try:
                    step[key] = self.resolve(
                        step[key], step["params"], step["vars"])
                except Exception as error:
                    step["errors"].append(self._resolve_error_(error))
                    step["success"] = False

        return step

    def _exec_all_(self, step, params, vars_):
        for substepdef in step["set"]:
            ret = self.exec_step(stepdef = substepdef,
                                 parent = step,
                                 params = step["params"],
                                 vars_ = step["vars"])
            if not ret["success"]:
                step["success"] = False

    def _exec_and_(self, step, params, vars_):
        for substepdef in step["set"]:
            ret = self.exec_step(stepdef = substepdef,
                                 parent = step,
                                 params = step["params"],
                                 vars_ = step["vars"])
            if not ret["success"]:
                step["success"] = False
                break

    def _exec_eval_(self, step, params, vars_):
        # Clear retval
        try:
            self._save_retval_(step)
        except Exception:
            pass

        try:
            step["result"] = eval(step["script"])
            if step["result"]:
                self._check_result_(step)
                self._save_retval_(step)
        except Exception as error:
            msg = "Invalid eval script"
            try:
                msg = msg + ": `%s`" %(step["script"])
                msg = msg + ", " + str(error)
            except Exception:
                pass

            step["errors"].append(self._resolve_error_(SpyteError(msg)))
            step["success"] = False

    def _exec_func_(self, step, params, vars_):
        # Clear retval
        try:
            self._save_retval_(step)
        except Exception:
            pass

        try:
            try:
                func = self.resolve(step["paths"][-1])
                assert hasattr(func, '__call__')

            except Exception:
                msg = "Path does not resolve to a callable function"
                try:
                    msg = msg + ": %r" %(step["paths"][-1])
                except Exception:
                    pass
                raise SpyteError(msg)

            try:
                step["result"] = func(*step["args"], **step["kwargs"])
            except Exception as e:
                msg = "Function execution failed"
                try:
                    msg = msg + " for %s" %(step["paths"][-1])
                except Exception:
                    pass
                
                try:
                    msg = msg + ": %r" %(e)
                except Exception:
                    pass
                
                raise SpyteError(msg)
                    
            self._check_result_(step)
            self._save_retval_(step)

        except Exception as error:
            msg = "Function execution failed"
            try:
                msg = msg + ": %s" %(step["paths"][-1])
                msg = msg + ", " + str(error)
            except Exception:
                pass

            step["errors"].append(self._resolve_error_(SpyteError(msg)))
            step["success"] = False

    def _exec_if_(self, step, params, vars_):
        try:
            step["result"] = eval(step["cond"])
        except Exception as error:
            msg = "Invalid condition"
            try:
                msg = msg + ": `%s`" %(step["cond"])
                msg = msg + ", " + str(error)
            except Exception:
                pass
            step["errors"].append(self._resolve_error_(SpyteError(msg)))

        if step["result"]:
            step["success"] = self.exec_step(stepdef = step["substep"],
                                             parent = step,
                                             params = step["params"],
                                             vars_ = step["vars"])["success"]

    def _exec_loop_(self, step, params, vars_):
        # Calculate end time
        endtime = None
        try:
            if step["timeout"] != None:
                endtime = time.time() + step["timeout"]
        except Exception as error:
            msg = "Invalid timeout value"

            try:
                msg = msg + ": `%r`" %(step["timeout"])
            except Exception:
                pass

            try:
                msg = msg + ": `%r`" %(error)
            except Exception:
                pass

            step["errors"].append(self._resolve_error_(SpyteError(msg)))
            step["success"] = False

        # Perform loop
        if step["success"] != False:
            while True:
                ret = self.exec_step(stepdef = step["substep"],
                                     parent = step,
                                     params = step["params"],
                                     vars_ = step["vars"])
                if ret["success"]:
                    step["success"] = True
                    break
                        
                if endtime != None and time.time() > endtime:
                    msg = "Loop timed out"
                    try:
                        msg = msg + " after %r seconds" %(timeout)
                    except Exception:
                        pass

                    step["errors"].append(
                        self._resolve_error_(SpyteError(msg)))
                    step["success"] = False
                    break

    def _exec_or_(self, step, params, vars_):
        step["success"] = False
        for substepdef in step["set"]:
            ret = self.exec_step(stepdef = substepdef,
                                 parent = step,
                                 params = step["params"],
                                 vars_ = step["vars"])
            if ret["success"]:
                step["success"] = True
                break

    def _exec_step_(self, step, params, vars_):
        step["success"] = self.exec_step(stepdef = step["substep"],
                                         parent = step,
                                         params = step["params"],
                                         vars_ = step["vars"])["success"]
                

    def get_steps_by_name(self, path, names = []):
        steps = {}
        defs = []

        for name in names:
            steps[name] = []

        for path in self._get_deps_(path = path):
            ref = self._parser_.esc_split(
                self._parser_.parse_ref(path)[1], ".")
            if ref[0] == "defs":
                if not ref[1] in defs:
                    defs.append(ref[1])
                    for name in names:
                        if name in self.defs[ref[1]]:
                            steps[name].append(
                                self._resolve_path_(
                                    "<:defs.%s.%s>" %(ref[1], name)))
        return steps

    def exec_step(self,
                  path = None,
                  stepdef = None,
                  parent = None,
                  params = {},
                  vars_ = {},
                  perform_meta = False):

        init_path = path

        if not init_path:
            if "path" in stepdef:
                init_path = stepdef["path"]
            elif "paths" in stepdef:
                init_path = stepdef["paths"][-1]

        # Get init and cleanup steps if relevant
        meta_steps = {}

        if init_path:
            if parent == None and perform_meta:
                meta_steps = self.get_steps_by_name(init_path,
                                                    names = ["init", "clean"])

        # Perform init steps
        if "init" in meta_steps:
            for meta_step in meta_steps["init"]:
                self.exec_step(
                    stepdef = meta_step,
                    params = params,
                    vars_ = vars_)

        step = self._resolve_step_(path, stepdef, parent, params, vars_)

        try:
            self.preexec_cb(step)
        except Exception as e:
            if DEBUG:
                print "Pre execution callback failed: %r" %(e)

        if step["success"] != False:
            step["success"] = True
                    
            if step["type"] == "all":
                self._exec_all_(step, params, vars_)

            elif step["type"] == "and":
                self._exec_and_(step, params, vars_)

            elif step["type"] == "eval":
                self._exec_eval_(step, params, vars_)

            elif step["type"] == "func":
                self._exec_func_(step, params, vars_)

            elif step["type"] == "if":
                self._exec_if_(step, params, vars_)

            elif step["type"] == "loop":
                self._exec_loop_(step, params, vars_)

            elif step["type"] == "or":
                self._exec_or_(step, params, vars_)

            elif step["type"] == "step":
                self._exec_step_(step, params, vars_)

            else:
                success = False
                msg = "Unknown test step type"
                try:
                    msg = msg + ": `%s`" %(repr(step))
                except Exception:
                    pass
                errors.append(self._resolve_error_(SpyteError(msg)))

        try:
            self.postexec_cb(step)
        except Exception as e:
            if DEBUG:
                print "Post execution callback failed: %r" %(e)

        # Perform cleanup steps
        if "clean" in meta_steps:
            for meta_step in meta_steps["clean"]:
                self.exec_step(
                    stepdef = meta_step,
                    params = params,
                    vars_ = vars_)

        return step

    def preexec_cb(self, stepdef = None):
        '''
        Optional callback (set by __init__()) called just prior to executing
        a test step of type `func`, `eval`, or `step`.

        @param stepdef -  The JSON dictionary which defines the test step that
                          is about to be run.
        '''
        pass

    def postexec_cb(self, stepdef = None):
        '''
        Optional callback (set by __init__()) called just after executing a
        test step of type `func`, `eval`, or `step`.

        @param stepdef -  The JSON dictionary which defines the test step that
                          is about to be run.
        '''
        pass

    def resolve(self, attr, params = {}, vars_ = {}):
        return self._parser_.resolve(attr, {
            "attr": self.attr,
            "defs": self.defs,
            "params": params,
            "vars": vars_,
            "retvals": self.retvals})

    def run(self, path = None, filepath = None, **kwargs):
        '''
        Executes the function defined in self._testdef_ or a test step defined
        in a *.json test def file within self.defpath if the "path" parameter
        is given. If "path" is not given but kwargs are, they will override the
        params given in the test defintion file. When self is initialized all
        def files become keys in the self.defs dictionary with their values
        being the parsed JSON object in the body of the test def file.

        @param path - (Optional if self._testdef_ is defined) The path to a
                      test step in a test def file, defined using "defs", the
                      name of the file (without the file extension), and the
                      name of one of the steps defined therein as follows:
                      "defs.<filename>.<stepname>"

        @param filepath - (Optional) Path to a *.json file containing a single
                          teststep which defines the test to be run.

        @param kwargs - An unbound number of key-value pairs representing the
                        parameters that are to be passed to this test step.

        @return This function returns a JSON-like dictionary containing the
                following information:

                {
                  "type": "step",
                  "path": <string_with_path_to_step_definition>,
                  "desc": <string_with_description_from_definition>,
                  "params": <dict_with_key_value_pairs_passed_to_step>,
                  "success": <True/False>,
                  "errors": [
                    {
                      "type": <name_of_exception_type>,
                      "msg": <descriptive_exception_msg>
                    },
                    ...
                  ]
                  "substeps": <list_of_substep_results>
                }
                
                Where <list_of_substep_results> is an ordered list containing
                the same result dictionary for each substep executed as part
                of this step. All test steps should eventually resolve to a
                function call. The result dictionary of function calls differs
                as follows:
                
                {
                  "type": "func",
                  "path": <string_with_path_to_step_definition>,
                  "args": <list_with_unnamed_parameters_passed_to_func>,
                  "kwargs": <dict_with_named_parameters_passed_to_func>,
                  "expected_result": <expected_result_of_function>,
                  "result": <actual_result_of_function>,
                  "success": <True/False>,
                  "errors": [
                    {
                      "type": <name_of_exception_type>,
                      "msg": <descriptive_exception_msg>
                    },
                    ...
                  ]
                }
        '''
        if filepath:
            testdef = self._add_testdef_(filepath = filepath)
            if len(testdef) != 1:
                msg = "Test definition doesn't contain exactly one test step"
                try:
                    msg = msg + ": " + str(filepath)
                except Exception:
                    pass
                raise SpyteError(msg)

            path = "<:defs.%s.%s>" %(self._get_filename_(filepath),
                                  testdef.keys()[0])

        return self.exec_step(
            path = path, params = kwargs, perform_meta = True)
