# What is spyte?

"Spyte" (pronounced "spite") is an acronym for "(S)imple (py)thon (t)est
(e)xecutor". And is primarily intended to provide a suite of tools that may
be used to quickly generate system-level test cases (as opposed to unit test
cases).

Spyte is most suited to automated regression testing of an agile system or
project where project requirements are not completely defined up front making
it necessary for the test suite to expand over time and for new test cases to
be easily defined using existing building blocks.

The primary feature of spyte is the test executor class (executor.TestExecutor)
which combines user-defined classes and JSON formatted test definition files to
form the building blocks upon which test cases can be defined and executed.

## Architecture


```
            ┌───────────────────┐
            │  Parameters  and  │
            │     Variables     │
            │('params' & 'vars')│
            └───┬───────────┬───┘
                │           │
           ┌────┘           └─┐         ┌─────┐
           ▼                  ▼         ▼     │
┌─────────────────────┐   ┌─────────────────┐ │
│     User-Defined    │   │    JSON Test    │ │
│Classes and Functions├──►│ Step Definition ├─┘
│      ('attrs')      │   │     ('defs')    │
└─────────────────────┘   └─────────────────┘
           ▲                       ▲
           │  ┌─────────────────┐  │
           │  │  Return Values  │  │
           └─►│                 │◄─┘
              │   ('retvals')   │
              └─────────────────┘ 
```

# Building Test Suites

A test suite is a colection of test cases that each verify the functionality of
a specific feature or behavior. Each test case is made up of individual test
steps that define specific opperations to perform.

Spyte abstracts test suites and test cases as a hierarchy of test steps in
which individual test steps execute a sequence of other test steps.

## Definition and Execution of Test Steps

Test steps are defined in JSON and are parsed and executed by a TestExecutor
object as defined in executor.py. TestExecutor objects are initialized with the
a test definition path and an unbound number of key-value pairs.

The path must point to a directory containing definitions of the test steps
that the caller wishes to use. The format of these files will be described in
greater detail below, but each file contains a list of test steps.

The key-value pairs must contain either a function or a python object (the
value) with a name which will uniquely identify it as an attribute of the
TestExecutor class (the key).

The functions/objects passed in by the user become attributes of
TestExecutor.attr. Test definitions are parsed into python dictionaries and
added by filename to the python dictionary in TestExecutor.defs.

For example, suppose the caller defines several test steps in two files named 
"my_first_test_steps.json" and "my_second_test_steps.json" in a direcroty named
"my_dir/testdefs". Suppose that these test steps depend on a function named
"myFunction()" as well as several functions within two custom classes:
"MyFirstClass" and "MySecondClass".

The TestExecutor object might be initialized as follows:
```
  e = TestExecutor('my_dir/testdefs',
                   func1 = myFunction,
                   attr1 = MyFirstClass(),
		   attr2 = MySecondClass())
```

The variable `e` will now contain the following::
* `e.attr.func1()` (which is actually myFunction())
* `e.attr.attr1` (with access to all of MyFirstClass's functions/attributes)
* `e.attr.attr2` (with access to all of MySecondClass's functions/attributes)
* `e.defs["my_first_test_steps"] (which contains all the user-defined test
  steps of my_first_test_steps.json)
* `e.defs["my_second_test_steps"] (which contains all the user-defined test
  steps of my_second_test_steps.json)

Each of these functions, attributes, and test steps are now uniquely defined by
a path which is a dot-delimited string beginning with either "attr" or "defs",
as in the following examples:
* "attr.func1" points to `e.attr.func1()`
* "attr.attr1.some_func" points to "some_func()" in `e.attr.attr1`
* "attr.attr2.some_other_func" points to "some_other_func()" in `e.attr.attr2`
* "defs.my_first_test_steps.some_step" points to 
  `e.defs["my_first_test_steps"]["some_step"]
* "defs.my_second_test_steps.some_other_step" points to 
  `e.defs["my_second_test_steps"]["some_other_step"]

There are four callback functions that can be passed to the TestExecutor
`__init__()` function as well: `_prestep_cb_`, `_poststep_cb_`, `_prefunc_cb_`,
`_postfunc_cb_`. If defined, these functions are called just prior to executing
a test step or function and just prior to the test step or function's
completion, respectively. These functions must accept the following input
parameters:

* `_prestep_cb_`
  * `stepdef`: A python dictionary containing the parsed JSON object which
    defined this test step in the *.json test definition file. 
  * `paths`: A python list containing the path to the current test step as well
    as the paths to each of its ancestors. The path to the initial test step is
    paths[0] and the current step is at paths[-1]
  * `params` - A python dictionary containing the names and values of the
    parameters passed to this test step by it's parent or by the `run()`
    function.
* `_poststep_cb_` 
  * `stepdef`: A described above. 
  * `paths`: A described above.
  * `result`: The result dictionary for this test step as described below.
* `_prefunc_cb_`
  * `paths`: The paths leading to this function, as described above.
  * `args`: An ordered list of the values to be passed to the function as the
    *args vector.
  * `kwargs`: A python dictionary containing the parameter names and values to
    be passed to the function as the **kwargs dictionary.
  * `expected_result`: The result (or a subset of it) that the function is
    expected to be returned by the function.
* `_postfunc_cb_`
  * `paths`: As described above.
  * `result`: The function result dictionary described below.

### Running a Test Step

Once `e` is initialized, the function `e.run(<path>, **kwargs)` may be called
to executes a test step, where:
* <path> is the unique path to a function or test step as described above.
* **kwargs is an unbound number of key-value pairs of any paramters that must
  be passed to the function or test step.

### Processing the Result of a Test Step

The result of the "run" function is the following JSON-like python dictionary:
```
{
  "type": "step",
  "path": <string_with_path_to_step_definition>,
  "desc": <string_with_description_from_definition>,
  "params": <dict_with_key_value_pairs_passed_to_step>,
  "success": <True/False>,
  "errors": [
    {
      "type": <name_of_exception_type>,
      "msg": <descriptive_exception_msg>
    },
    ...
  ]
  "substeps": <list_of_substep_results>
}
```

Where <list_of_substep_results> is an ordered list containing the same result
dictionary for each substep executed as part of this step. All test steps
should eventually resolve to a function call. The result dictionary of function
calls differs as follows:

```
{
  "type": "func",
  "path": <string_with_path_to_step_definition>,
  "args": <list_with_unnamed_parameters_passed_to_func>,
  "kwargs": <dict_with_named_parameters_passed_to_func>,
  "expected_result": <expected_result_of_function>,
  "result": <actual_result_of_function>,
  "success": <True/False>,
  "errors": [
    {
      "type": <name_of_exception_type>,
      "msg": <descriptive_exception_msg>
    },
    ...
  ]
}
```

All functions refered to by "path" in the "run" function or any of the
functions defined within its substeps must raise an Exception() that provides
a descriptive message regarding the failure that has occurred and must return a
JSON representable value such as a Boolean, number, string, list, or dict. The
`expected_response` must be an exact match for Boolean, numeric and string
values, but can be a subset of list or dict values. In the case of a list, the
values of the expected subset must be in the same order as the actual return
values. In the case of a dict, all the keys in the expected subset must be
present in the actual response and their values must follow the rules pertinent
to its type. If a function raises an Exception or the expected result
is not contained within the return value, then it's considered to have failed
(i.e. `success`=`False`), otherwise it's considered to have succeeded.

### Defining a Test Step

#### Test Step Definition Files

The definitions of individual test steps must be contained within a *.json file
in the directory used to initialize the TestExecutor. Each file must contain a
single JSON object with the name and definition of each test step as follows:
```
 {
   "some_step": <step_def>,
   "some_other_step": <step_def>
 }
```

Where `<step_def>` is a JSON object which provides the following information:
```
  {
    "desc": <desc>,
    "params": <dflt_params>,
    "substep": <substep>
  }
```

Where:
* `<desc>` is a string containing a brief, user-friendly description of the
  test step
* `<dflt_params>` is a JSON object containing key-value pairs which describe
  all the parameters that might be passed to this test step and their default
  values.
* `<substep>` is a JSON array which sequentially defines the individual
  opperations to be performed by this step, as described below

#### Test Substeps

Each test step performs a substep represented as a JSON object. Substeps are
differentiated from one another by the `type` key which may be any one of the
following:
* `and` type substeps contain a `set` key which is a JSON array containing
  any number of the substep JSON objects described in this section. If any of
  the substeps in this array fail, the remaining steps are not run and the
  parent step is considered to have failed.
* `or` type substeps contain a `set` key which is a JSON array containing
  any number of the substep JSON objects described in this section. If any of
  the substeps in this array succeed, the remaining steps are not run and the
  parent step is considered to have succeeded.
* `all` type substeps contain a `set` key which is a JSON array containing
  any number of the substep JSON objects described in this section. If any of
  the substeps in this array fail, the remaining steps are still run but the
  parent step is considered to have failed.
* `step` type substeps run another test step as defined by the following
  additional key-value pairs:
  * `path` is the path to the test step as described above.
  * `params` is a JSON object containg a key-value pair for each parameter to
    be passed to the test step. The key is the name of each parameter, and the
    value is a parameter JSON dictionary as described below.
* `func` type substeps call a function as defined by the following:
  * `path` is the path to the function as described above.
  * `args` is a JSON array containing a parameter JSON dictionary, as described
    below, for each parameter being passed to the function without explicitly
    naming the parameter (i.e. the `*args` list)
  * `kwargs` is a JSON object containg a key-value pair for each parameter to
    be passed to the function by name. The key is the name of the parameter,
    and the value is a parameter JSON dictionary as described below.
  * `expected_result` is either null (if the return value of the function is
    not important) or is the string, number, boolean, list, or dictionary which
    the function must return to be considerred a success. Note that in the case
    of strings, numbers, and booleans, the value must be exactly equal for the
    function to be considered a success. In the case of lists, the
    expected result must be a properly ordered subset of the elements of the
    actual result list according to the rules described here for each element's
    type. In the case of a dictionary, all the keys of the expected result must
    be present in the actual result and their values must follow the rules
    described here for their type.

#### Function and Test Step Parameter Definitions

Though parameters are ultimately passed to a function or test step as a simple
JSON object containing a key-value pair for each parameter (or an array of
values as the *args list passed to a function), the value of each pair is
represented by another JSON object which accounts for the source of the
parameter's value. These nested JSON objects each contain a "type" key which is
one of the following:
* `byref` - In this case, the value of the parameter is to be derived by
  referencing from the parent test step or the kwargs passed to the `run()`
  function. Parameter objects of this contain a `ref` key which provides the
  name of the parent parameter to be used.
* `byval` - In this case, the value of the parameter is defined explicitly by
  the `val` key.

If a parameter which is defined in a test step is omitted when it is referenced
by another step, the default value given in the original test step definition
will be used.

### Example

For a working example that brings together all the components described herein,
refer to samples/scripts/simple_math.py and samples/testdefs/simple_math.json.