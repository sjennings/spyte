from setuptools import setup, find_packages

setup(
    name =           'spyte',
    version =        '0.0.3',
    description =    'Simple Python Test Executor',
    author =         'Sean Jennings',
    author_email =   'seanrjennings@gmail.com',
    license =        'MIT',
    url =            'https://bitbucket.org/sjennings/spyte',
    download_url =   'https://bitbucket.org/sjennings/spyte/get/master.tar.gz',
    classifiers =   ['Development Status :: 3 - Alpha',
                     'Intended Audience :: Developers',
                     'Topic :: Software Development :: Quality Assurance',
                     'Programming Language :: Python :: 2.7'],
    keywords =      ['test', 'executive', 'development'],
    packages =       find_packages(),
    install_requires = [],
)
